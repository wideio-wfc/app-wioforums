# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
import wioframework.fields as models

from wioframework.amodels import *
from wioframework import decorators as dec
from extfields import fields as ef

@wideio_publishable()
@wideiomodel
class Forum(models.Model):
    title = models.CharField(max_length=255, blank=False)
    image = models.ForeignKey('references.Image')

    def get_all_references(self, st):
        return list(self.topic_set.all())
    # managed by STAFF

    class WIDEIO_Meta:
        NO_DRAFT = True
        permissions = dec.all_permissions_from_request_rwa(
            lambda r: True,
            lambda r: r.user.is_staff,
            lambda r: r.user.is_staff)
        MOCKS={
          'default':[
            {
             '@id': 'forum-0000',
             'title': 'Test Demo Forum'
            }
          ]
        }


@wideio_publishable()
@wideio_owned()
@wideiomodel
class Topic(models.Model):
    """
    A topic of discussion on the forum. It is a simple question on which people starts to
    discuss
    """
    forum = models.ForeignKey(Forum)
    subject = models.CharField(max_length=255, blank=False)
    question = models.TextField(blank=False)
    replies = ef.RerankableParagraphField()
    raise_ticket = models.BooleanField(default=False)

    def get_all_references(self, st):
        # FIXME: THIS IS WRONG
        return []

    # managed by STAFF
    class WIDEIO_Meta:
        ADD_CAPTCHA = True
        permissions = dec.all_permissions_from_request_rwa(
            lambda r: True,
            lambda r: r.user.is_authenticated(),
            lambda r: r.user.is_authenticated())
        MOCKS={
          'default':[
            {
             '@id': 'forum-topic-0000',
             'forum': 'forum-0000',
             'title': 'Test Demo Forum',
             'subject': 'Why mirror swaps left and right but not up and down ?',
             'replies': []
            }
          ]
        }

MODELS = [Forum, Topic]
